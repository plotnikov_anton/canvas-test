function Figure(options) {
    this.x = options.x || 0;
    this.y = options.y || 0;
    this.dx = 2;
    this.dy = 2;
    this.new = true;
    self = this;
    this.color = self.setColor();
    this.type = options.type;
}

/**
 * @method Figure.prototype.setColor
 *
 * @description Method to generate a random hexadecimal
 * value representing a {@link Figure} instance colour
 *
 * @returns {string}    - hexadecimal colour representation
 */
Figure.prototype.setColor = function () {

    let rgb = 0;
    let color = "#";

    for (let i = 0; i < 3; i++) {
        rgb = (Math.floor(Math.random() * 255)).toString(16);
        color += rgb;
    }
    return color.trim();
};

/**
 * @method Figure.prototype.setSize
 *
 * @description Non-recursive method to generate a random
 * size to be assigned to a {@link Figure} instance
 * Minimum size is chosen as 15
 *
 * @returns {number}    - size
 */
Figure.prototype.setSize = function () {
    let size = Math.floor(Math.random() * 50);
    while (size < 15) {
        size = Math.floor(Math.random() * 50);
    }
    return size;
};

function Ball(options) {
    Figure.apply(this, arguments);
    this.radius = self.setSize();
}

Ball.prototype = Object.create(Figure.prototype);
Ball.prototype.constructor = Ball;

function Square(options) {
    Figure.apply(this, arguments);
    this.side = self.setSize();
}

Square.prototype = Object.create(Figure.prototype);
Square.prototype.constructor = Square;

function Factory() {
}

Factory.prototype.type = Ball;

/**
 * @method Factory.prototype.create
 *
 * @description Factory method to create a new figure instance
 *
 * @param options - defines the list of options for
 * a new {@link Figure} instance
 * @return {*}    - instance of an appropriate class
 */
Factory.prototype.create = function (options) {
    switch (options.type) {
        case "ball":
            this.type = Ball;
            break;
        case "square":
            this.type = Square;
            break;
    }
    return new this.type(options);
};

let factory = new Factory();

let canvas = document.getElementById("canvas");
let context = canvas.getContext("2d");

function drawFigure(figure) {

    context.beginPath();

    switch (figure.type) {
        case "ball":
            context.arc(figure.x, figure.y, figure.radius, 0, Math.PI * 2);
            break;
        case "square":
            context.rect(figure.x, figure.y, figure.side, figure.side);
            break;
    }

    context.fillStyle = figure.color;
    context.fill();
    context.globalAlpha = 1;
    context.closePath();
}

/**
 * A function to check whether a figure is newly created
 * or not to ensure the need of walls collision checking
 *
 * @param figure        - a {@link Figure} instance
 * @returns {boolean}   - flag
 */
function isNewFigure(figure) {
    if ((figure.x > figure.radius && figure.y > figure.radius) || figure.type === "square") {
        figure.new = false;
    }
    return figure.new;
}

/**
 * A function to check collisions with walls
 *
 * @param figure    - a {@link Figure} instance
 */
function defineWallCollision(figure) {

    if (!isNewFigure(figure)) {

        if ((figure.x + figure.dx > canvas.width - figure.radius || figure.x + figure.dx < figure.radius) ||
            (figure.x + figure.dx > canvas.width - figure.side || figure.x + figure.dx < 0)) {
            figure.dx = -figure.dx;
        }
        if ((figure.y + figure.dy > canvas.height - figure.radius || figure.y + figure.dy < figure.radius) ||
            (figure.y + figure.dy > canvas.height - figure.side || figure.y + figure.dy < 0)) {
            figure.dy = -figure.dy;
        }
    }
}

let figureArray = [];

for (let i = 0; i < 20; i++) {
    if (i % 2 === 0) {
        figureArray[i] = factory.create({type: "ball"});
    } else {
        figureArray[i] = factory.create({type: "square"});
    }
}

function draw() {

    // Probably faster than context.clearRect(0, 0, width, height)
    canvas.width = canvas.width;

    figureArray.forEach((item, index) => {
        setTimeout(() => {
            drawFigure(item);
            defineWallCollision(item);
            item.x += item.dx;
            item.y += item.dy;
        }, index * 2000);
    });
}

setInterval(draw, 20);


